mod embedded_redis_client;
mod embedded_redis_server;
mod error;

pub use crate::embedded_redis_client::EmbeddedRedisClient;
pub use crate::embedded_redis_client::EmbeddedRedisClientSettings;
pub use crate::embedded_redis_server::{
    RedisServerConnectionSettings, RedisServerSettings, RedisServerStartupSettings,
};

#[cfg(test)]
mod tests;

#[cfg(test)]
pub use tests::{embedded_redis_client_settings_for_testing, redis_server_settings_for_testing};
