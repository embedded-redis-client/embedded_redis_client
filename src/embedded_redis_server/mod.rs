pub mod embedded_redis_server;
mod settings;
pub mod states;
pub mod util;

use embedded_redis_server::connection;
// use util;

pub use embedded_redis_server::EmbeddedRedisServer;
pub use settings::{
    RedisServerConnectionSettings, RedisServerSettings, RedisServerStartupSettings,
};

//NOTE: type checking on emum values is not yet possible in rust, but might become available in the future
// https://github.com/rust-lang/rfcs/pull/2593

// TODO: move to mod.rs, and rename to redis_server.rs
