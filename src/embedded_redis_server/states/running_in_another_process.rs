use redis;
use redis::ConnectionLike;

use crate::embedded_redis_server::states::CheckingRedisServerEnvironment;
use crate::error::{EmbeddedRedisClientError, ErrorType};

pub struct RedisServerRunningInAnotherProcess {
    connection: redis::Connection,
}

impl RedisServerRunningInAnotherProcess {
    pub fn try_from(
        environment: CheckingRedisServerEnvironment,
    ) -> Result<Self, EmbeddedRedisClientError> {
        match environment.redis_server_connection {
            Some(connection) => {
                if connection.is_open() {
                    Ok(RedisServerRunningInAnotherProcess{
                        connection: connection,
                    })
                } else {
                    Err(EmbeddedRedisClientError::new(ErrorType::ServerEnvironment, String::from("Connection is not open")))
                }
            },
            None => {
                Err(EmbeddedRedisClientError::new(ErrorType::Other, String::from("Cannot create RedisServerRunningInAnotherProcess from RedisServerRunningInAnotherProcess without a valid connection")))
            }
        }
    }
}
