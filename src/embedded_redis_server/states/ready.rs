// use std::process::{Child};
use redis;

use crate::embedded_redis_server::connection::try_to_get_redis_server_connection_until_timeout;
use crate::embedded_redis_server::states::StartedRedisServerProcess;
use crate::embedded_redis_server::{RedisServerSettings};
use crate::error::{EmbeddedRedisClientError};

pub struct ReadyRedisServerProcess {
    server_process: StartedRedisServerProcess,
    client_connection: redis::Connection,
}

impl Drop for ReadyRedisServerProcess {
    fn drop(&mut self) {
        let shutdown_result: redis::RedisResult<()> =
            redis::cmd("SHUTDOWN").query(&mut self.client_connection);
        if let Err(error) = shutdown_result {
            println! {"Warning: redis process did not shutdown as expected. Some data may have been lost.\n"};
            println! {"{}\n,",error}
        }
    }
}

impl ReadyRedisServerProcess {
    pub fn new(
        started_redis_server: StartedRedisServerProcess,
        server_settings: RedisServerSettings,
    ) -> Result<Self, EmbeddedRedisClientError> {
        let client_connection = try_to_get_redis_server_connection_until_timeout(
            server_settings.connection,
            server_settings.startup.startup_timeout,
        )?;
        let ready_server_process = ReadyRedisServerProcess {
            server_process: started_redis_server,
            client_connection,
        };
        Ok(ready_server_process)
    }
}
