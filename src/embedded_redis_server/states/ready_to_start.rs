use crate::embedded_redis_server::states::CheckingRedisServerEnvironment;

pub struct ReadyToStart {
    _is_initialzed: bool,
}

impl ReadyToStart {
    pub fn from(_environment: CheckingRedisServerEnvironment) -> Self {
        ReadyToStart {
            _is_initialzed: true,
        }
    }
}
