use crate::error::EmbeddedRedisClientError;

pub struct FaultyRedisServer {
    error: EmbeddedRedisClientError,
    // faulty_redis_server: Box<RedisServer>,
}

impl FaultyRedisServer {
    pub fn new(error: EmbeddedRedisClientError) -> Self {
        FaultyRedisServer { error }
    }

    pub fn get_error(&self) -> &EmbeddedRedisClientError {
        &self.error
    }
}

// implement From for the other RedisServer values
