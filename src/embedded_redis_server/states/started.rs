use std::path::PathBuf;
use std::process::{Child, Command, Stdio};

use crate::embedded_redis_server::states::ReadyToStart;
use crate::error::EmbeddedRedisClientError;
use crate::RedisServerStartupSettings;

pub struct StartedRedisServerProcess {
    child_process: Child,
}

impl Drop for StartedRedisServerProcess {
    fn drop(&mut self) {
        self.child_process.kill().unwrap();
    }
}

impl StartedRedisServerProcess {
    pub fn start(
        redis_server_ready_to_start: ReadyToStart,
        startup_settings: RedisServerStartupSettings,
    ) -> Result<Self, EmbeddedRedisClientError> {
        let child_process = start_redis_server(
            startup_settings.path_redis_server_executable,
            startup_settings.path_redis_server_configuration_file,
        )?;

        Ok(StartedRedisServerProcess { child_process })
    }
}

fn start_redis_server(
    path_redis_server_executable: PathBuf,
    path_redis_configuration: PathBuf,
) -> Result<Child, EmbeddedRedisClientError> {
    let mut working_directory = path_redis_server_executable.clone();
    working_directory.pop();

    let redis_process = Command::new(path_redis_server_executable.clone())
        .current_dir(working_directory.clone())
        .arg(path_redis_configuration.clone())
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn();
    // .expect("Failed to start redis server");

    if cfg!(debug_assertions) {
        println!("Started redis server process: {:?}\n", redis_process);
    }
    // thread::sleep(time::Duration::from_secs_f32(0.5));
    match redis_process {
        Ok(child) => {
            // Ok(mut child) => {
            // if cfg!(debug_assertions) {
            //     // #![allow(unused_must_use)]
            //     thread::Builder::new()
            //         .name("cairn-redis".to_string())
            //         .spawn(move || {
            //             let output_reader = BufReader::new(child.stdout.take().unwrap());
            //             output_reader.lines().for_each(|line|println!("{}",line.unwrap()));
            //         });
            //     // println!("stderr: {:?}",String::from_utf8(child.stderr.take().unwrap());
            //     println!("stdout: {:?}\n",child.stdout.take().unwrap());
            // }
            if cfg!(debug_assertions) {
                println!("Created redis process child: {:?}\n", child);
            }
            Ok(child)
        }
        Err(error) => Err(EmbeddedRedisClientError::from(error)),
    }
}
