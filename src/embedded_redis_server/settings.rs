use std::net::SocketAddr;
use std::path::PathBuf;

use serde::Deserialize;
use std::time::Duration;

#[derive(Debug, Clone, Deserialize)]
pub struct RedisServerSettings {
    pub startup: RedisServerStartupSettings,
    pub connection: RedisServerConnectionSettings,
}

#[derive(Deserialize, Clone, Debug)]
pub struct RedisServerStartupSettings {
    pub path_redis_server_executable: PathBuf,
    pub path_redis_server_configuration_file: PathBuf,
    pub startup_timeout: Duration,
    pub maximum_startup_retries: u8,
}

#[derive(Deserialize, Debug, Clone)]
pub struct RedisServerConnectionSettings {
    pub server_address: SocketAddr,
    pub database_id: u16,
}

impl redis::IntoConnectionInfo for RedisServerConnectionSettings {
    fn into_connection_info(self) -> redis::RedisResult<redis::ConnectionInfo> {
        let connection_address = redis::ConnectionAddr::Tcp(
            format! {"{}",self.server_address.ip().clone()},
            self.server_address.port().clone(),
        );

        let connection_info = redis::ConnectionInfo {
            addr: Box::new(connection_address),
            db: i64::from(self.database_id.clone()),
            passwd: None,
            username: None,
        };

        redis::RedisResult::Ok(connection_info)
    }
}
