use crate::embedded_redis_server::RedisServerSettings;

#[derive(Debug, Clone)]
pub struct EmbeddedRedisClientSettings {
    pub redis_server_settings: RedisServerSettings,
}
