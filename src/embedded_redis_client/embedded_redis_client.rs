use redis;

use super::states::{FaultyRedisClient, ReadyRedisClient, StartedRedisClient};
use crate::error::{EmbeddedRedisClientError, ErrorType};
use crate::EmbeddedRedisClientSettings;

pub enum EmbeddedRedisClient {
    Ready(ReadyRedisClient),
    Faulty(FaultyRedisClient),
}

impl EmbeddedRedisClient {
    pub async fn start(
        settings: EmbeddedRedisClientSettings,
    ) -> Result<EmbeddedRedisClient, EmbeddedRedisClientError> {
        let started_redis_graph_client = StartedRedisClient::new(settings)?;
        let ready_redis_graph_client = ReadyRedisClient::new(started_redis_graph_client).await?;
        Ok(EmbeddedRedisClient::Ready(ready_redis_graph_client))
    }

    pub fn to_faulty(&mut self) -> () {
        match self {
            EmbeddedRedisClient::Ready(ready_client) => {
                let settings = ready_client.get_settings().clone();
                drop(ready_client);
                let faulty_client = FaultyRedisClient::new(settings);
                *self = EmbeddedRedisClient::Faulty(faulty_client);
            }
            EmbeddedRedisClient::Faulty(_) => (),
        }
    }

    pub async fn execute_async<T>(
        &mut self,
        command: redis::Cmd,
    ) -> Result<T, EmbeddedRedisClientError>
    where
        T: redis::FromRedisValue,
    {
        match self {
            EmbeddedRedisClient::Ready(ready_client) => {
                let mut connection = ready_client.get_async_redis_connection();
                match command.query_async(&mut connection).await {
                    Ok(result) => Ok(result),
                    Err(error) => {
                        println!("Query failed with error: {:?}\n", error);
                        if !ready_client.check_connection() {
                            let settings = ready_client.get_settings().clone();
                            self.retry_execute_async(command, settings).await
                        } else {
                            if cfg!(debug_assertions) {
                                println!("Unexpected error after query: {:#?}", error);
                            }
                            Err(EmbeddedRedisClientError::from(error))
                        }
                    }
                }
            }
            EmbeddedRedisClient::Faulty(faulty_client) => Err(EmbeddedRedisClientError::new(
                ErrorType::Other,
                String::from("Auto-recovery from a faulty client not implemented"),
            )),
        }
    }

    async fn retry_execute_async<T>(
        &mut self,
        command: redis::Cmd,
        settings: EmbeddedRedisClientSettings,
    ) -> Result<T, EmbeddedRedisClientError>
    where
        T: redis::FromRedisValue,
    {
        if cfg!(debug_assertions) {
            println!("-----RETRY_EXECUTE_ASYNC-----");
            println!("Current process id: {}", std::process::id());
            match self {
                EmbeddedRedisClient::Ready(ready_client) => {
                    println!(
                        "retry_execute_async started with a ready client with server state {}\n",
                        ready_client.server_state()
                    )
                }
                EmbeddedRedisClient::Faulty(_) => {
                    println!("retry_execute_async started with a faulty client")
                }
            }
        }
        self.to_faulty();
        match Self::start(settings).await {
            Ok(client) => {
                *self = client;
                match self {
                    EmbeddedRedisClient::Ready(ready_client) => {
                        let mut connection = ready_client.get_async_redis_connection();
                        match command.query_async(&mut connection).await {
                            Ok(result) => Ok(result),
                            Err(error) => Err(EmbeddedRedisClientError::from(error)),
                        }
                    }
                    EmbeddedRedisClient::Faulty(faulty_client) => Err(EmbeddedRedisClientError::new(
                        ErrorType::Other,
                        String::from(
                            "Failed to restart EmbeddedRedisClient during retry-execution attempt",
                        ),
                    )),
                }
            }
            Err(error) => {
                Err(EmbeddedRedisClientError::new(
                    ErrorType::ServerStartup(Box::new(error)),
                    String::from(
                        "Failed to restart EmbeddedRedisClient during retry-execution attempt",
                    ),
                ))
                // match self {
                //     EmbeddedRedisClient::Ready(ready) => {
                //         // let faulty_client = FaultyEmbeddedRedisClient::from_ready(*ready);
                //         *self = EmbeddedRedisClient::Faulty(FaultyEmbeddedRedisClient::from_ready(*ready));
                //         Err(RedisClientError::new(ErrorType::ServerStartup(Box::new(error)), String::from("Failed to restart EmbeddedRedisClient during retry of async query execution")))
                //     }
                //     _ => Err(RedisClientError::new(ErrorType::Other, String::from("Unexpected client state")))
                // }
            }
        }
    }
}

// impl Drop for EmbeddedRedisClient {
//     fn drop(&mut self) {
//         if let Some(redis_child_process) = &mut self.server_process {
//             let shutdown_result: redis::RedisResult<()> =
//                 block_on(redis::cmd("SHUTDOWN").query_async(&mut self.client_connection));
//             redis_child_process.kill();
//         }
//     }
// }

// pub fn start_services(startup_settings: RedisStartupSettings) -> Result<Child, RedisClientError>{
//     run_redis_server(
//         startup_settings.path_redis_server_executable.clone(),
//         startup_settings.path_redis_server_configuration_file.clone(),
//     );
// }

#[cfg(test)]
mod tests {
    use super::*;
    // use rand::{distributions::Uniform, Rng};
    use async_std::task;
    use rand::Rng;

    use crate::embedded_redis_client_settings_for_testing;

    async fn new_embedded_redis_client() -> EmbeddedRedisClient {
        // kill_process_occupying_target_tcp_port(
        //     redis_graph_client_settings.connection.server_address.port(),
        // );

        let embedded_redis_client =
            EmbeddedRedisClient::start(embedded_redis_client_settings_for_testing())
                .await
                .unwrap();
        return embedded_redis_client;
    }

    #[test]
    fn create_redis_client() {
        let _embedded_redis_client = new_embedded_redis_client();
    }

    #[async_std::test]
    async fn test_redis_connection() {
        let mut embedded_redis_client = new_embedded_redis_client().await;

        let test_key = "test_connection";
        let mut rng = rand::thread_rng();
        let value_to_write: u16 = rng.gen();

        let write_command = redis::Cmd::set(test_key, value_to_write);
        let _: () = embedded_redis_client
            .execute_async(write_command)
            .await
            .unwrap();

        let get_command = redis::Cmd::get(test_key);
        let retrieved_value = embedded_redis_client
            .execute_async(get_command)
            .await
            .unwrap();

        let delete_command = redis::Cmd::del(test_key);
        let _: () = embedded_redis_client
            .execute_async(delete_command)
            .await
            .unwrap();

        assert_eq!(value_to_write, retrieved_value);
    }

    #[async_std::test]
    async unsafe fn test_parallel_redis_connections() {
        let number_of_instances = 3;
        let mut tasks = Vec::with_capacity(number_of_instances);

        let mut rng = rand::thread_rng();
        let values_to_write: Vec<u16> = (0..number_of_instances)
            .map(|_| rng.gen_range(1..u16::MAX))
            .collect();

        for value_to_write in values_to_write.into_iter() {
            tasks.push(task::spawn(async move {
                let mut embedded_redis_client = new_embedded_redis_client().await;

                let test_key = format!("test_connection_{}", value_to_write.clone());

                let write_command = redis::Cmd::set(&test_key, value_to_write.clone());
                let _: () = embedded_redis_client
                    .execute_async(write_command)
                    .await
                    .unwrap();

                let get_command = redis::Cmd::get(&test_key);
                let retrieved_value: u16 = embedded_redis_client
                    .execute_async(get_command)
                    .await
                    .unwrap();

                let delete_command = redis::Cmd::del(&test_key);
                let _: () = embedded_redis_client
                    .execute_async(delete_command)
                    .await
                    .unwrap();

                assert_eq!(value_to_write.clone(), retrieved_value);
            }))
        }

        task::block_on(async {
            for t in tasks {
                t.await;
            }
        });
    }
}
