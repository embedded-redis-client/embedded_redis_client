// use crate::redis_server::RedisServer;
// use crate::redisgraph_client::states::ReadyRedisGraphClient;
use crate::embedded_redis_client::EmbeddedRedisClientSettings;

#[derive(Debug)]
pub struct FaultyRedisClient {
    settings: EmbeddedRedisClientSettings,
    // redis_server: RedisServer,
}

impl FaultyRedisClient {
    pub fn new(settings: EmbeddedRedisClientSettings) -> Self {
        FaultyRedisClient { settings }
    }
    // pub fn from_ready(ready_client: ReadyRedisGraphClient) -> Self {
    //     FaultyRedisGraphClient {
    //         settings: ready_client.get_settings().clone(),
    //     }
    // }
}
