use redis;

use crate::embedded_redis_client::states::started::StartedRedisClient;
use crate::embedded_redis_server::EmbeddedRedisServer;
use crate::error::EmbeddedRedisClientError;
use crate::EmbeddedRedisClientSettings;

pub struct ReadyRedisClient {
    settings: EmbeddedRedisClientSettings,
    redis_server: EmbeddedRedisServer,
    redis_client: redis::Client,
    redis_server_connection: redis::aio::MultiplexedConnection,
}

impl ReadyRedisClient {
    pub async fn new(
        started_redis_graph_client: StartedRedisClient,
    ) -> Result<ReadyRedisClient, EmbeddedRedisClientError> {
        // let redis_server_connection = started_redis_graph_client.get_connection()?;
        let redis_server_connection = started_redis_graph_client
            .redis_client
            .get_multiplexed_async_std_connection()
            .await?;

        Ok(ReadyRedisClient {
            settings: started_redis_graph_client.settings,
            redis_server: started_redis_graph_client.redis_server,
            redis_client: started_redis_graph_client.redis_client,
            redis_server_connection,
        })
    }

    pub fn check_connection(&self) -> bool {
        match self.redis_client.get_connection() {
            Ok(_) => true,
            Err(_) => false,
        }
        // connection.is_open() && connection.check_connection()  <= not implemented for async connection
    }

    pub fn get_async_redis_connection(&mut self) -> redis::aio::MultiplexedConnection {
        return self.redis_server_connection.clone();
    }
    pub fn get_settings(&self) -> &EmbeddedRedisClientSettings {
        &self.settings
    }
    pub fn server_state(&self) -> String {
        self.redis_server.state()
    }
}

// impl TryFrom<StartedRedisGraphClient> for ReadyRedisGraphClient {
//     type Error = RedisGraphClientError;

//     fn try_from(started_redis_graph_client: StartedRedisGraphClient) -> Result<ReadyRedisGraphClient, RedisGraphClientError> {
//         // let redis_server_connection = started_redis_graph_client.get_connection()?;
//         let redis_server_connection = started_redis_graph_client.redis_client.

//         Ok(ReadyRedisGraphClient {
//             redis_server_connection,
//             ..started_redis_graph_client
//         })
//     }
// }

// let client = redis::Client::open(settings.connection.clone())?;
// let server_process;
// let client_connection;
// if cfg!(debug_assertions) {
//     println!("Redis client: {:?}\n", client);
// }

// // let client_connection = block_on(client.get_multiplexed_async_connection());
// match block_on(client.get_multiplexed_async_connection()) {
//     Ok(connection) => {
//         client_connection = connection;
//         server_process = None;
//     }
//     Err(_) => {
//         // kill_process_occupying_target_tcp_port(
//         //     settings.connection.server_address.port(),
//         // );
//         server_process = Some(start_redis_server(
//             settings.startup.path_redis_server_executable.clone(),
//             settings
//                 .startup
//                 .path_redis_server_configuration_file
//                 .clone(),
//         )?);
//         thread::sleep(time::Duration::from_secs_f32(2.0));
//         client_connection = block_on(client.get_multiplexed_async_connection())?;
//     }
// }
// if cfg!(debug_assertions) {
//     println!("Redis server process: {:?}\n", server_process);
// }

// Ok(RedisGraphClient {
//     settings: settings,
//     server_process: server_process,
//     client: client,
//     client_connection: client_connection,
// })
