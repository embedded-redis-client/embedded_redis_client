use redis;

use crate::embedded_redis_client::EmbeddedRedisClientSettings;
use crate::embedded_redis_server::EmbeddedRedisServer;
use crate::error::EmbeddedRedisClientError;

pub struct StartedRedisClient {
    pub settings: EmbeddedRedisClientSettings,
    pub redis_server: EmbeddedRedisServer,
    pub redis_client: redis::Client,
}

impl StartedRedisClient {
    pub fn new(
        settings: EmbeddedRedisClientSettings,
    ) -> Result<StartedRedisClient, EmbeddedRedisClientError> {
        let redis_server =
            EmbeddedRedisServer::start_with_retry(settings.redis_server_settings.clone())?;
        let redis_client = redis::Client::open(settings.redis_server_settings.connection.clone())?;

        Ok(StartedRedisClient {
            settings,
            redis_server,
            redis_client,
        })
    }
}
