// Look here for an example on how to implement error types: https://doc.rust-lang.org/src/std/io/error.rs.html#42
use std::error::Error;
use std::fmt;
use std::io;

use redis;

#[derive(Debug)]
pub struct EmbeddedRedisClientError {
    pub error_type: ErrorType,
    pub context: String,
    // source: Option<Box<(dyn error::Error)>>,
}

#[derive(Debug)]
pub enum ErrorType {
    IO(io::Error),
    Redis(redis::RedisError),
    ServerTimeout,
    ServerStartup(Box<EmbeddedRedisClientError>),
    ServerEnvironment,
    Other,
}

impl EmbeddedRedisClientError {
    pub fn new(error_type: ErrorType, context: String) -> Self {
        Self {
            error_type: error_type,
            context: context,
        }
    }
}

impl Error for EmbeddedRedisClientError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self.error_type {
            ErrorType::IO(error) => Some(error),
            _ => None,
        }
    }
}

impl fmt::Display for EmbeddedRedisClientError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // REVIEW: is this match useful?
        match &self.error_type {
            ErrorType::IO(_err) => writeln!(f, "Context:\n{}", &self.context)?,
            _ => writeln!(f, "Context:\n{}", &self.context)?,
        };

        match &self.source() {
            Some(err) => writeln!(f, "Source error:\n{}", err)?,
            &None => (),
        }
        Ok(())
    }
}

impl From<std::io::Error> for EmbeddedRedisClientError {
    fn from(error: std::io::Error) -> EmbeddedRedisClientError {
        EmbeddedRedisClientError {
            error_type: ErrorType::IO(error),
            context: String::new(),
        }
    }
}

impl From<redis::RedisError> for EmbeddedRedisClientError {
    fn from(error: redis::RedisError) -> EmbeddedRedisClientError {
        EmbeddedRedisClientError {
            error_type: ErrorType::Redis(error),
            context: String::new(),
        }
    }
}
