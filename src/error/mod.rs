mod error;

pub use error::{EmbeddedRedisClientError, ErrorType};
