use std::env;
use std::net::{IpAddr, Ipv4Addr, SocketAddr};

use crate::embedded_redis_server::{
    RedisServerConnectionSettings, RedisServerSettings, RedisServerStartupSettings,
};
use crate::EmbeddedRedisClientSettings;

pub fn embedded_redis_client_settings_for_testing() -> EmbeddedRedisClientSettings {
    EmbeddedRedisClientSettings {
        redis_server_settings: redis_server_settings_for_testing(),
    }
}

pub fn redis_server_settings_for_testing() -> RedisServerSettings {
    println!(
        "RedisGraphClient current directory: {:?}\n",
        env::current_dir()
    );
    let mut database_base_directory = env::current_dir().unwrap();
    // database_base_directory.pop();
    database_base_directory.pop();
    database_base_directory.push("database");
    database_base_directory.push("redis");

    let mut path_redis_server_executable = database_base_directory.clone();
    path_redis_server_executable.push("redis");
    path_redis_server_executable.push("redis-server");

    let mut path_redis_server_configuration_file = database_base_directory.clone();
    path_redis_server_configuration_file.push("configuration");
    path_redis_server_configuration_file.push("redis.conf");

    let mut path_redis_insight_executable = database_base_directory.clone();
    path_redis_insight_executable.push("redis-insight");
    path_redis_insight_executable.push("redisinsight");

    let redis_server_startup_timeout = std::time::Duration::from_secs(7);
    let maximum_number_of_redis_server_startup_attempts = 3;

    let redis_startup_settings = RedisServerStartupSettings {
        path_redis_server_executable: path_redis_server_executable,
        path_redis_server_configuration_file: path_redis_server_configuration_file,
        startup_timeout: redis_server_startup_timeout,
        maximum_startup_retries: maximum_number_of_redis_server_startup_attempts,
        // path_redis_insight_executable: path_redis_insight_executable,
    };

    let redis_connection_settings = RedisServerConnectionSettings {
        server_address: SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 6379),
        database_id: 0,
    };

    let redis_server_settings = RedisServerSettings {
        startup: redis_startup_settings,
        connection: redis_connection_settings,
    };
    println!("{:?}\n", redis_server_settings);

    return redis_server_settings;
}
