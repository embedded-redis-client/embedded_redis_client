# Embedded Redis Client

## Purpose
EmbeddedRedisClient automatically runs a redis-server instance during the life of the client application. Using EmbeddedRedisClient, multiple client instances can communicate with the redis-server instance in parallel.

If another redis server instance is already running on the configured port, then EmbeddedRedisClient connects to this already running server instead.

## Unit tests
The unit tests of this crate expect the presence a redis-server binary and redis.conf file in the following locations relative to the crate root:
 ../database/redis/redis/redis-server
 ../database/redis/configuration/redis.conf

The unit tests try to connect to the redis server on redis' default port localhost/6379

## Compatibility
Tested on Ubuntu 18.04 and macOS 11.2.

## Limitations
Simultaneous starting of multiple EmbeddedRedisClient instances may fail.